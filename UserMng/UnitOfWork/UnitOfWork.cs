﻿using System;
using UserMng.Context;
using UserMng.Models;
using UserMng.Services;

namespace UserMng.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private UserMngDbContext _context;

        public UnitOfWork(UserMngDbContext context)
        {
            _context = context;
            InitReponsitory();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
        
        public IGenericReponsitory<Student> StudentReponsitory { get; private set; }
        
        public  IGenericReponsitory<Class> ClassReponsitory { get; private set; }

        private void InitReponsitory()
        {
            StudentReponsitory = new GenericReponsitory<Student>(_context);
            ClassReponsitory = new GenericReponsitory<Class>(_context);
        }
    }
}