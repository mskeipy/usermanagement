﻿using System.Threading.Tasks;
using UserMng.Context;
using UserMng.Models;
using UserMng.Services;

namespace UserMng.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericReponsitory<Student> StudentReponsitory { get; }
        IGenericReponsitory<Class> ClassReponsitory { get; }
        void SaveChanges();
    }
}