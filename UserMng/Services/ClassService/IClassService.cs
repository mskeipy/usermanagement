﻿using System.Collections.Generic;
using UserMng.Models;

namespace UserMng.Services
{
    public interface IClassService
    {
        IEnumerable<Class> Get();
        Class GetId(int? Id);
        void Add(Class  classes);
        void Delete(int id);
        void Edit(Class classes);
        void Save();
    }
}