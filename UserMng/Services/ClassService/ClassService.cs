﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;
using UserMng.Models;
using UserMng.ViewsModel.Class;
using UserMng.ViewsModel.Student;

namespace UserMng.Services
{
    public class ClassService : IClassService
    {
        private readonly IClassReponsitory _classReponsitory;

        public ClassService(IClassReponsitory classReponsitory)
        {
            _classReponsitory = classReponsitory;
        }
        public IEnumerable<Class> Get()
        {
            return _classReponsitory.Get();
        }

        public Class GetId(int? Id)
        {
            return _classReponsitory.GetId(Id);
        }

        public void Add(Class classes)
        {
            _classReponsitory.Add(classes);
            
        }

        public void Delete(int id)
        {
            _classReponsitory.Delete(id);
           
        }

        public void Edit(Class classes)
        {
           _classReponsitory.Edit(classes);
           
        }

        public void Save()
        {
            _classReponsitory.Save();
        }
    }
}