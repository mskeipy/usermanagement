﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;
using UserMng.Models;
using UserMng.ViewsModel.Student;
using UserMng.UnitOfWork;

namespace UserMng.Services
{
    public class StudentService : IStudentService
    {
        private IStudentReponsitory _studentReponsitory;

        public StudentService(IStudentReponsitory studentReponsitory)
        {
            _studentReponsitory = studentReponsitory;
        }
        public IEnumerable<Student> Get()
        {
            return _studentReponsitory.Get();
        }

        public Student GetId(int? Id)
        {
            return _studentReponsitory.GetId(Id);
        }

        public void Add(Student student)
        {
            _studentReponsitory.Add(student);     
        }

        public void Delete(int id)
        {
            _studentReponsitory.Delete(id);    
        }

        public void Edit(Student student)
        {
            _studentReponsitory.Edit(student);   
        }

        public void Save()
        {
            _studentReponsitory.Save();
        }
    }
    

}
