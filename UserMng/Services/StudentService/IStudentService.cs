﻿using System.Collections.Generic;
using UserMng.Context;
using UserMng.Models;

namespace UserMng.Services
{
    public interface IStudentService
    {
        IEnumerable<Student> Get();
        Student GetId(int? Id);
        void Add(Student  student);
        void Delete(int id);
        void Edit(Student student);
        void Save();
    }
}