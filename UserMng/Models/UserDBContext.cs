﻿using Microsoft.EntityFrameworkCore;
using UserMng.Models;
using UserMng.ViewsModel.Student;


namespace UserMng.Context  
{  
    public class UserMngDbContext : DbContext  
    {  
        public UserMngDbContext(DbContextOptions options) : base(options)  
        {  
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
    }  
}    