﻿using Microsoft.EntityFrameworkCore;
using UserMng.Context;

namespace UserMng.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    UserId = 1,
                    UserName = "admin@gmail.com",
                    Password = "123456",
                    Role = 1
                },
                new User
                {
                    UserId = 2,
                    UserName = "phi@gmail.com",
                    Password = "123456",
                    Role = 0
                }
            );
            modelBuilder.Entity<Student>().HasData(
                new Student
                {
                    StudentId = 1,
                    StudentName = "nguyen A",
                    ClassId = 1,
                }
                
            );
            modelBuilder.Entity<Class>().HasData(
                new Class
                {
                    ClassId = 1,
                    Classname = "1A"
                }
                
            );
            
        }
    }
}