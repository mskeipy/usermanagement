﻿using System;
    
    
namespace UserMng.Context  
{  
    public class User
    {  
        public int UserId { get; set; }  
        public string UserName { get; set; }  
        public string Password { get; set; }
        public int Role { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
    }  
} 