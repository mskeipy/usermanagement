﻿using System.Collections.Generic;
using UserMng.Context;
using UserMng.Models;

namespace UserMng.Models
{
    public class Class
    {
        public int ClassId { get; set; }
        public string Classname { get; set; }
        
        public List<Student> Students { get; set; }
    }
}