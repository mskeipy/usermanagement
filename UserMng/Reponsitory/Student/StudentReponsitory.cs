﻿using UserMng.Context;

namespace UserMng.Services
{
    public class StudentReponsitory : GenericReponsitory<Student>, IStudentReponsitory
    {
        public StudentReponsitory(UserMngDbContext context) : base(context)
        {
        }
    }
}