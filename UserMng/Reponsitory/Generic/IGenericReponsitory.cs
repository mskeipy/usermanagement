﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace UserMng.Services
{
    public interface IGenericReponsitory<TEntity>
    {
        IEnumerable<TEntity> Get();
        TEntity GetId(int? Id);
        void Add(TEntity  tentity);
        void Delete(int id);
        void Edit(TEntity tentity);
        void Save();

    }
}