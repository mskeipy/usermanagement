﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using UserMng.Context;


namespace UserMng.Services
{
    public class GenericReponsitory<TEntity> : IGenericReponsitory<TEntity> where TEntity : class 
    {
        private readonly UserMngDbContext _context;

        public GenericReponsitory(UserMngDbContext context)
        {
            _context = context;
        }

        public  IEnumerable<TEntity> Get()
        {
            return _context.Set<TEntity>();
        }
        
        /// <summary>
        /// add TEntity service    
        /// </summary>
        /// <param name="addTEntity">set<Tentity>()</param>
        public void Add(TEntity addTEntity)
        {
            _context.Set<TEntity>().Add(addTEntity);
            _context.SaveChanges();

        }
  
        public void Save()
        {
            _context.SaveChanges();
        }
        
        /// <summary>
        /// get TEntity service    
        /// </summary>
        /// <param name="id">TEntity</param>
        public TEntity GetId(int? Id)
        {
            return _context.Set<TEntity>().Find(Id); 

        }
        
        public void Edit(TEntity editTEntity)
        {
            _context.Set<TEntity>().Update(editTEntity);
            _context.SaveChanges();

        }
        
        public void Delete(int Id)
        {
            try
            {
                TEntity tentity = _context.Set<TEntity>().Find(Id);
                _context.Set<TEntity>().Remove(tentity);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
    }
}