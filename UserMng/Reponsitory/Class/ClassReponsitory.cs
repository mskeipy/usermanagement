﻿using UserMng.Context;
using UserMng.Models;

namespace UserMng.Services
{
    public class ClassReponsitory : GenericReponsitory<Class>, IClassReponsitory
    {
        public ClassReponsitory(UserMngDbContext context) : base(context)
        {
        }
    }
}