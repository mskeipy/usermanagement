﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using UserMng.ViewsModel.Class;
using UserMng.ViewsModel.Student;


namespace UserMng.ViewsModel
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Context.Student, StudentViewsModel>();
            CreateMap<Models.Class, ClassViewsModel>();
        }
    }
}