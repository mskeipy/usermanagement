﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace UserMng.ViewsModel.Student
{
    public class StudentViewsModel
    {
        
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        
        public int ClassId { get; set; }
        public Models.Class Class { get; set; }
    }
}