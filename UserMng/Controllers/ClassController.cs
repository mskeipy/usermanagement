﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserMng.Context;
using UserMng.Models;
using UserMng.Services;
using UserMng.UnitOfWork;

namespace UserMng.Controllers
{
    public class ClassController : Controller
    {
        
        private readonly IClassService _classService;

        public ClassController(IClassService classService)
        {
            _classService = classService;
        }
        // GET
        public IActionResult Index()
        {
            return
            View(_classService.Get());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public RedirectToActionResult Create(Class classes)
        {
            _classService.Add(classes);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ViewResult Edit(int Id)
        {
            var getClassId = _classService.GetId(Id);
            return View(getClassId);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Class classes)
        {
            _classService.Edit(classes);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int Id)
        {
            _classService.Delete(Id);
            return RedirectToAction("Index");
        }
    }
}