﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserMng.Context;
using UserMng.Services;
using UserMng.UnitOfWork;
using UserMng.ViewsModel.Student;

namespace UserMng.Controllers
{
    public class StudentController : Controller
    {
        
        private readonly IStudentService _studentService;
        public StudentController(IStudentService  studentService)
        {
            _studentService = studentService;
        }
        
        /// <summary>
        /// show index student
        /// </summary>
        /// <returns>viewstudent</returns>   
        public ViewResult Index()
        {
            return  View(_studentService.Get());
            
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post create student
        /// </summary>
        /// <param name="student"> StudentViewsModel </param>
        /// <returns>index else View</returns>
        [HttpPost]
        public RedirectToActionResult Create(Student student)
        {   
                _studentService.Add(student);
                return RedirectToAction("Index");
        }

        /// <summary>
        /// get id student
        /// </summary>
        /// <param name="id"> StudentViewsModel </param>
        /// <returns>view edit</returns>
        [HttpGet]
        public ViewResult Edit(int id)
        {
            var getStudentId = _studentService.GetId(id);
            return View(getStudentId);
        }

        /// <summary>
        /// Post edit student
        /// </summary>
        /// <param name="student"> StudentViewsModel </param>
        /// <returns>index else View</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(Student student)
        {
           
            _studentService.Edit(student);
            return RedirectToAction("Index");           
        }


        /// <summary>
        /// get delete
        /// </summary>
        /// <param name="id">student</param>
        /// <returns>index student</returns>
        [HttpGet]
        public IActionResult Delete(int Id)
        {

            _studentService.Delete(Id);
            return RedirectToAction("Index");
        } 
    }
}